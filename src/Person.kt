abstract class Person (var name:String, var surname:String, var gpa:Double){
    constructor(name:String, surname:String): this(name,surname,0.0){
    }
    abstract fun goodBoy(): Boolean
    open fun getDetails(): String{
        return "$name $surname has score $gpa"
    }
}


