fun main(args: Array<String>) {
    val x:Int = 1
    val y:Int = 2
    val z = x + y
    println("Hello world $z")

    //Nullable
    var data: Int
    var dataNullable: Int?
    data = 2
    println(y == data)
    println(y === data)
    dataNullable = null
//    data = dataNullable
}