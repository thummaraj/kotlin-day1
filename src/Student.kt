class Student(name:String,surname:String,gpa:Double, var department: String): Person(name,surname,gpa){
    override fun getDetails(): String {
        return super.getDetails() + "and study in $department"
    }

    override fun goodBoy(): Boolean {
        return gpa > 2.00
    }
}
fun main(args: Array<String>){
    val nol: Student = Student("Sonchai", "Pitak", 2.00,"CAMT")
    val no2: Student = Student("Somying", "tak", 3.00,"CMU")
    val no3: Student = Student("Somsri", "Pi", 4.00,"COOP")
    println(nol.getDetails())
    println(no2.getDetails())
    println(no3.getDetails())
}
