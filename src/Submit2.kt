fun main(args: Array<String>){
    helloMe()
    helloMe("Krittayanee", "Thummaraj")
    helloMe("Krittayanee")
    helloMe(name = "Krittayanee")
    helloMe(surname = "Thummaraj")

}
fun helloMe(name:String = "no name", surname:String = "no surname"): Unit {
    println("name: $name surname: $surname")
}