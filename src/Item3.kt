fun main(args: Array<String>){
    helloMore("Prayuth")
    helloMore("Sanchai")
    gradeReport("somchai",3.33)
    gradeReport()
    gradeReport(name = "nobita")
    gradeReport(gpa = 2.50)
}
fun helloMore(text: String): Unit{
    println("Hello $text")
}
//fun gradeReport(name: String = "annonymous",
//                gpa:Double = 0.00): Unit{
//    println("minister $name gpa: is $gpa")
//}
fun gradeReport(name: String = "annonymous",
                gpa:Double = 0.00): Unit = println("minister $name gpa: is $gpa")
