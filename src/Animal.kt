abstract class Animal(var onOgLegs:Int, var food: String){
    abstract fun getSound(): String

}

class Dog(noOfLegs: Int, food: String, var name: String) : Animal(noOfLegs, food) {
    override fun getSound(): String {
        return "$name say Box Box"
    }
}

class Lion(noOfLegs: Int, food: String) : Animal(noOfLegs, food) {
    override fun getSound(): String {
        return "Roarrrr"
    }
}

fun main(array: Array<String>) {
    val dog: Dog = Dog(4, "Bond", "Jack")
    val lion: Lion = Lion(4, "Zebra")
    println(dog.getSound())
    println(lion.getSound())
}