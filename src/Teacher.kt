data class Teacher(var name:String, var courseName:String, var shirtColor: Color)

fun main(args: Array<String>){
    var teacher1: Teacher = Teacher("Somsak","Democratic", Color.RED)
    var teacher2: Teacher = Teacher("Somsak","Democratic", Color.BLUE)
    var teacher3: Teacher = Teacher("Somsak", "Mathmatics", Color.GREEN)
    val (teacherName, teacherCourseName) = teacher3

    println(teacher1)
    println(teacher2)
    println(teacher3)
    println(teacher1.equals(teacher3))
    println(teacher1.equals(teacher2))
    println(teacher1.name)
    println(teacher1.courseName)
    println(teacher1.component1())
    println(teacher1.component2())
    println("$teacherName teaches $teacherCourseName")
}

enum class Color{
    RED,BLUE,GREEN
}