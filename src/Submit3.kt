fun main(args: Array<String>){
    println(isOdd(4))
    println(isOdd(5))
    println(isOdd(6))
    println(isOdd(7))
}
fun isOdd(value: Int): String {
    if(value.rem(2) == 0) {
        return "$value is even value"
    } else {
        return "$value is odd value"
    }
}