fun main(args: Array<String>){

    println(getTranscript(40))
    println(getTranscript(60))
    println(getTranscript(75))
    println(getTranscript(90))
    println(getTranscript(1000))

}
fun getAbbreviation(grade: Char): String {
    when (grade) {
        'A' -> return "Abnormal"
        'B' -> return "Bad Boy"
        'C' -> return "Fantastic"
        'F' -> {
            println("not smart")
            return "Cheap"
        } else -> return "Hello"
    }
}

fun getGrade(score: Int): String? {
    var grade: String?
    when(score){
        in 0..50 -> grade = "F"
        in 51..70 -> grade = "C"
        in 71..80 -> grade = "B"
        in 80..100 -> grade = "A"
        else -> grade = null
    }
    return grade
}
fun getTranscript(score: Int): String? {
    var score:Int = score
    var grade: String? = getGrade(score)

    if(grade!==null){
        var g:Char = grade.toCharArray()[0]
        return getAbbreviation(g)
    }

    return "not found"




}