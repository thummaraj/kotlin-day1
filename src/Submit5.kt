fun findMaxValue(values: Array<Int>):Int{
    var max: Int = 0
    for(i in values.indices){
        if(values[i] > max){
            max = values[i]
        }
    }
    return max
}

fun main(args: Array<String>){
    var arrays = arrayOf(5,55,200,1,3,5,7)
    var max = findMaxValue(arrays)
    println("max value is $max")
}